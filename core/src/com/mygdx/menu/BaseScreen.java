package com.mygdx.menu;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;




public abstract class BaseScreen  implements Screen,InputProcessor  {
	
	//stage principal
    protected Table uiTable;
    protected String screenAnterior;
	protected Stage mainStage;
    protected Stage uiStage;
    
    InputMultiplexer revulsivo;
    
public BaseScreen() {
	
	
	int vpwidth=Gdx.graphics.getWidth();
	int vpheight=Gdx.graphics.getHeight();
    
    
        mainStage = new Stage();
        uiStage = new Stage();
 
        uiTable = new Table();
        uiTable.setFillParent(true);
        uiStage.addActor(uiTable);
        initialize();
    }

    
    

    public abstract void initialize();

    public abstract void update(float dt);

    public void render(float dt) {
    	//fps
        dt = Math.min(dt, 1/30f);
        
       
        uiStage.act(dt);
        mainStage.act(dt);  
        update(dt);
     
      
    	Gdx.gl.glClearColor(0.4f, 0.5f, 0.8f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        mainStage.draw();
       uiStage.draw();
     
    }

    public void resize(int width, int height) {}

    public void pause() {}

    public void resume() {}

    public void dispose() {}

    /**
     * Se lleva a cabo cuando la pantalla se convierte en la activa
     */
    public void show() {  
    	
        InputMultiplexer im = (InputMultiplexer)Gdx.input.getInputProcessor();
        im.addProcessor(this);
        im.addProcessor(uiStage);
        im.addProcessor(mainStage);
   
      
    }

    /**
     * Se lleva a cabo cuando la pantalla deja de estar activa
     */
    public void hide() {  
    	  InputMultiplexer im =  (InputMultiplexer)Gdx.input.getInputProcessor();
        im.removeProcessor(this);
        im.removeProcessor(uiStage);
        im.removeProcessor(mainStage);
    	 
       
    }

    /**
     *  Useful for checking for touch-down events.
     */
    public boolean isTouchDownEvent(Event e)
    {
        return (e instanceof InputEvent) && ((InputEvent)e).getType().equals(Type.touchDown);
    }
    public boolean keyDown(int keycode){
    	
    	return false;  
    }

    public boolean keyUp(int keycode){
    	
    	return false;
    }

    public boolean keyTyped(char c) {
    	
    	return false;
    }

    public boolean mouseMoved(int screenX, int screenY){
    	
    	return false;
    }

    public boolean scrolled(int amount) {
    	
    	return false;
    	
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    	
    	return false;
    }

    public boolean touchDragged(int screenX, int screenY, int pointer) {
    	
    	return false;
    }

    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    	
    	return false;
    }





}
