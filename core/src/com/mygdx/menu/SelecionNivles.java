package com.mygdx.menu;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

public class SelecionNivles extends ApplicationAdapter {
	Stage stage;
	Skin skin;
	
	public SelecionNivles() {
		super();
		create();
		createBasicSkins();
		render();
		
	}
	public void create() {
		int buttonoffset=20;
		stage=new Stage();
		Gdx.input.setInputProcessor(stage);
		createBasicSkins();
		TextButton nivel1=new TextButton("1", skin);
		TextButton nivel2=new TextButton("2",skin);
		TextButton nivel3=new TextButton("3",skin);
		nivel1.setPosition(100,200);
		nivel1.addListener(new ClickListener(){
			public void clicked(InputEvent envent,float x, float y) {
			System.out.println("calvo");	
			}	
		});
		stage.addActor(nivel1);
		nivel2.setPosition(100,600);
		nivel2.addListener(new ClickListener(){
			public void clicked(InputEvent envent,float x, float y) {
			System.exit(0);
			}	
		});
		stage.addActor(nivel2);
		nivel3.setPosition(100,400);
		nivel3.addListener(new ClickListener(){
			public void clicked(InputEvent envent,float x, float y) {
			System.exit(0);
			}	
		});
		stage.addActor(nivel3);
		
		
	}
	/**
	 * personaliza un poco los botones
	 */
	private void createBasicSkins() {
		//creando fuente
		BitmapFont font=new BitmapFont();
		skin=new Skin();
		skin.add("default", font);
		//creando textura
		Pixmap pixmap=new Pixmap(Gdx.graphics.getWidth()/4, Gdx.graphics.getHeight()/10, Pixmap.Format.RGB888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("background", new Texture(pixmap));
		//estilo del boton
		TextButton.TextButtonStyle estilo=new TextButton.TextButtonStyle();
		estilo.up=skin.newDrawable("background",Color.GRAY);
		estilo.down=skin.newDrawable("background",Color.DARK_GRAY);
		estilo.checked=skin.newDrawable("background",Color.DARK_GRAY);
		estilo.over=skin.newDrawable("background",Color.BLUE);
		estilo.font=skin.getFont("default");
		skin.add("default", estilo);
		
	}
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act();
		stage.draw();
	}

}
