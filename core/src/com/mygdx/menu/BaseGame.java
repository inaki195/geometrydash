package com.mygdx.menu;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

/**
 * clase que extiende de game
 * crea fuente para el menu
 * @author inaki
 */
public abstract class BaseGame extends Game {
	private static BaseGame game;

    
  
    public BaseGame() {        
        game = this;
    }

    public void create() {
    	
        InputMultiplexer im = new InputMultiplexer();
        Gdx.input.setInputProcessor(im);

    
        
      
        
    }

    public static void setActiveScreen(BaseScreen s) {
        game.setScreen(s);
    }
}
