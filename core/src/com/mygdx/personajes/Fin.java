package com.mygdx.personajes;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Fin extends BaseActor {
	private Animation flagAnimation;
	public Fin(float x, float y, Stage s) {
		 super(x,y,s);
	       
	       String[] flagAnimationFiles = {"assets/items/flagGreen1.png", "assets/items/flagGreen2.png"};

	       flagAnimation = loadAnimationFromFiles(flagAnimationFiles, 0.2f, true);
	}

}
