package com.mygdx.personajes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ActorJugador extends Actor {
	private Texture jugador;
	boolean vivo;
	public  ActorJugador( Texture jugador) {
		this.jugador=jugador;
		setSize(jugador.getWidth(), jugador.getHeight());
		this.vivo=true;
	}
@Override
public void act(float delta) {
	super.act(delta);
	
	setX(getX()-20*delta);
}	
@Override
	public void draw(Batch batch, float parentAlpha) {
	batch.draw(jugador, getX(), getY());
	}
public Texture getJugador() {
	return jugador;
}
public void setJugador(Texture jugador) {
	this.jugador = jugador;
}
public boolean isVivo() {
	return vivo;
}
public void setVivo(boolean vivo) {
	this.vivo = vivo;
}

}
