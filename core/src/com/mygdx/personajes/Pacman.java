package com.mygdx.personajes;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Pacman extends BaseActor {
	private Animation animacion;
	public Pacman(float x, float y, Stage s) {
super(x, y, s);
		
		String[] enemy1AnimationFiles = {"pacman/1.png","pacman/2a.png"
				,"pacman/3a.png"};

	    animacion = loadAnimationFromFiles(enemy1AnimationFiles, 0.4f, true);
	}

}
