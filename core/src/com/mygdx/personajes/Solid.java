package com.mygdx.personajes;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Solid extends BaseActor {
	private boolean enebled;
	public Solid(float x, float y,float width,float height, Stage s) {
		super(x, y, s);
		setSize(width, height);
		   setBoundaryRectangle(); 
		   enebled=true;
	}
	public boolean isEnebled() {
		return enebled;
	}
	public void setEnebled(boolean b) {
		this.enebled = b;
	}


}
