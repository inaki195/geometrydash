package com.mygdx.personajes;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Actorcubo extends Actor {
	private Texture jugador;
	boolean alive;
	public  Actorcubo( Texture jugador) {
		this.jugador=jugador;
		setSize(jugador.getWidth(), jugador.getHeight());
		this.alive=true;
	}
@Override
public void act(float delta) {
	super.act(delta);
}	
@Override
	public void draw(Batch batch, float parentAlpha) {
	batch.draw(jugador, getX(), getY());
	}
public Texture getJugador() {
	return jugador;
}
public void setJugador(Texture jugador) {
	this.jugador = jugador;
}
public boolean isAlive() {
	return alive;
}
public void setAlive(boolean alive) {
	this.alive = alive;
}

}
