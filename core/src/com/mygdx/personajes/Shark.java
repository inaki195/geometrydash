package com.mygdx.personajes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Shark extends BaseActor {
	private Animation animacion;
	public Shark(float x, float y, Stage s) {
		super(x, y, s);

		 String[] enemy1AnimationFiles = {"shark/0.png", "shark/1.png","shark/2.png",
				"shark/3.png","shark/4.png"};
		   animacion = loadAnimationFromFiles(enemy1AnimationFiles, 0.1f, true);
	    	setAnimation(animacion);
		 
		
	

	   
	}
	  

}
