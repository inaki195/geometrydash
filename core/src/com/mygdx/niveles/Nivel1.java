
package com.mygdx.niveles;
import java.io.File;
import java.util.ArrayList;

import javax.swing.text.Position;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthoCachedTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.mygdx.Tiled.TilemapActor;
import com.mygdx.death.Finish;
import com.mygdx.game.Geometry;
import com.mygdx.menu.BaseScreen;
import com.mygdx.personajes.BaseActor;
import com.mygdx.personajes.Fin;
import com.mygdx.personajes.Pinchos;
import com.mygdx.personajes.Player;
import com.mygdx.personajes.Pacman;
import com.mygdx.personajes.Shark;
import com.mygdx.personajes.Solid;


public class Nivel1 extends BaseScreen{
		private  boolean gameOver;
		private TiledMap map;
	    private OrthographicCamera tiledCamera;
	    private OrthoCachedTiledMapRenderer tiledMapRenderer;
	    TilemapActor tiledMapActor;
	    Music song;
	    Player cubo;
	    MapObject startPoint;
	    MapProperties startProps;
	    int valor=0;
	    Sound deathSound;

@Override
public void initialize() {
	
	tiledMapActor = new TilemapActor("maps/mapa2.tmx", mainStage);
	
	deathSound = Gdx.audio.newSound(Gdx.files.internal("sound/roblox-death.mp3"));
	song = Gdx.audio.newMusic(Gdx.files.internal("music/TimeMachine.mp3"));
	song.setLooping(true);
	song.setVolume(0.6f);
	song.play();
	MapObject oj=new MapObject();
	ArrayList<Solid>solido=new ArrayList<Solid>();
	ArrayList<Shark>tiburon=new ArrayList<Shark>();
	ArrayList<Pinchos>pichos=new ArrayList<Pinchos>();
	ArrayList<Pacman>pacmans=new ArrayList<Pacman>();
    for (MapObject obj : tiledMapActor.getRectangleList("Solid") ) {
    	
        MapProperties props = obj.getProperties();
        Solid s;
        s=new Solid( (float)props.get("x"), (float)props.get("y"), (float)props.get("width"),
        		(float)props.get("height"), mainStage );
       solido.add(s);
             
    }
   
     startPoint = tiledMapActor.getRectangleList("inicio").get(0);
 
     startProps= startPoint.getProperties();
     cubo = new Player((float) startProps.get("x"), (float)startProps.get("y"), mainStage);
     
     for (MapObject obj : tiledMapActor.getobjectlist("pacman")) {
         MapProperties props = obj.getProperties();
         new Pacman((float)props.get("x"), (float)props.get("y"), mainStage);
       
         
     }
    
     
     for (MapObject obj : tiledMapActor.getobjectlist("shark")) {
    
         MapProperties props = obj.getProperties();
         
         Shark shark=new Shark((float)props.get("x"), (float)props.get("y"), mainStage);
       
         tiburon.add(shark);
     }

     for (MapObject obj : tiledMapActor.getTileList("pacman")) {
    	    
         MapProperties props = obj.getProperties();
         Pacman pacman=new Pacman((float)props.get("x"), (float)props.get("y"), mainStage);
         
       pacmans.add(pacman);
     }
 
     
     for (MapObject obj : tiledMapActor.getTileList("final")) {
    	 
         MapProperties props = obj.getProperties();
         new Fin((float)props.get("x"), (float)props.get("y"), mainStage);
         
     }
     
     for (MapObject obj : tiledMapActor.getTileList("pinchos")) {
         MapProperties props = obj.getProperties();
         Pinchos pinchos;
         pinchos=new Pinchos((float)props.get("x"), (float)props.get("y"), mainStage);
         pichos.add(pinchos);
     }
    
     cubo.toFront();
	
}

@Override
/**
 * control de colisiones
 */
public void update(float dt) {
	for (BaseActor actor : BaseActor.getList(mainStage, "com.mygdx.personajes.Solid"))
    {
        Solid solid = (Solid)actor;
        if ( cubo.overlaps(solid) && solid.isEnebled() )
        {
            Vector2 offset = cubo.preventOverlap(solid);

            if (offset != null)
            {
                // collided in X direction
                if ( Math.abs(offset.x) > Math.abs(offset.y) )
                    cubo.velocityVec.x = 0;
                else // collided in Y direction
                    cubo.velocityVec.y = 0;
            }
        }
    }
	//colision con el tiburon
	for (BaseActor pacman : BaseActor.getList(mainStage, "com.mygdx.personajes.Pacman"))
    {
		if (cubo.overlaps(pacman)) {
    		
    		deathSound.play(0.3f);
    		
    		song.pause();
           
    		Geometry.setActiveScreen(new Finish());
           
        }
    }
	 
	
} 
/**
 * metodo que hedera de la baseScreen detecta el teclado y taton
 */
public boolean keyDown(int keyCode)
{
    if (gameOver) {
    
        return false;
    }
   
    if (keyCode == Keys.SPACE)
    {
    	
       
        if ( Gdx.input.isKeyPressed(Keys.DOWN) )
        {
            
        }
        else if ( cubo.isOnSolid() )  
        {
        	String texture="";
        	
        	
        	
			cubo.cambiarimg(valor);
			valor++;
			if(valor>=4) {
				valor=0;
			}
            cubo.jump();
           
        }
    
 
}
    return false;
}



	
}
