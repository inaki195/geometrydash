package com.mygdx.game.desktop;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.g3d.particles.batches.BillboardParticleBatch.Config;
import com.mygdx.game.Geometry;
import com.mygdx.menu.Menu;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height=600;
		config.width=800;
		config.title="geometry dash";
		Game myGame=new Geometry();
		 LwjglApplication launcher = new LwjglApplication( myGame, "geometry Dash", 800, 640 );
	}
}
